from django.shortcuts import render
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from todos.models import TodoItem, TodoList
from django.urls import reverse_lazy

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todo_list"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todo"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["todo_item"] = TodoItem
    #     return context


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    success_url = reverse_lazy("list_todos")


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("list_todos")


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("list_todos")
    context_object_name = "todo"


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/newitem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", kwargs={"pk": self.object.list.pk})
        # need to format date:  2006-10-25 14:30:59
        # takes an arugment, has to be the id of what you want to load
        # while loading this page send key word arbuments
        # self.object is the item we just made
        # list in this model is connected to the ToDoList model
        # pk will pull the id of the object (to do list it is associated with)


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/edititem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", kwargs={"pk": self.object.list.pk})
